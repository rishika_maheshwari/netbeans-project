package javaapplication1;
 
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

public class JavaApplication1 {
    static String table1[], table2[];
    public static void main(String[] args) {
        // TODO code application logic here
        initTable();
		
		try {
			String currDir = System.getProperty("user.dir");
			FileReader f1 = new FileReader(currDir+"/datafile.txt");
			BufferedReader br = new BufferedReader(f1);
			
			
			FileWriter f2 = new FileWriter("results.txt");
			String line, output;
			while((line= br.readLine())!=null) {
				
				output = decodeLine(line);
				f2.write(output+"\n");
				
				System.out.println(output);
			}
			
			f2.close();
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	
    }
    private static void initTable() {
		table1 = new String[12];
		table1[0]="a";
		table1[1]="A";
		table1[2]="e";
		table1[3]="E";
		table1[4]="i";
		table1[5]="I";
		table1[6]="o";
		table1[7]="O";
		table1[8]="u";
		table1[9]="U";
		table1[10]="y";
		table1[11]="Y";
		
		table2 = new String[40];
		table2[0]="b";
		table2[1]="B";
		table2[2]="c";
		table2[3]="C";
		table2[4]="d";
		table2[5]="D";
		table2[6]="f";
		table2[7]="F";
		table2[8]="g";
		table2[9]="G";
		table2[10]="h";
		table2[11]="H";
		table2[12]="j";
		table2[13]="J";
		table2[14]="k";
		table2[15]="K";
		table2[16]="l";
		table2[17]="L";
		table2[18]="m";
		table2[19]="M";
		table2[20]="n";
		table2[21]="N";
		table2[22]="p";
		table2[23]="P";
		table2[24]="q";
		table2[25]="Q";
		table2[26]="r";
		table2[27]="R";
		table2[28]="s";
		table2[29]="S";
		table2[30]="t";
		table2[31]="T";
		table2[32]="v";
		table2[33]="V";
		table2[34]="w";
		table2[35]="W";
		table2[36]="x";
		table2[37]="X";
		table2[38]="z";
		table2[39]="Z";
		
	}

	private static String decodeLine(String line) {
		StringBuffer sb = new StringBuffer();
		
		String word[] = line.split("\\s+");
		
		for(int i=0;i<word.length;i++) {
			sb.append(decodeWord(word[i])+" ");
		}
		return sb+"";
	}

	private static String decodeWord(String word) {
		int len = word.length();
		StringBuffer sb = new StringBuffer();
		
		int pos[] = new int[len];
		
		int last=0;
		for(int i=len-1;i>=0;i--) {
			if(word.charAt(i)=='C' || word.charAt(i)=='V') {
				pos[i]=last;
				last=0;
			}else
				last++;
		}
		
		int number=0;
		for(int i=0;i<len;i++) {
			if(pos[i]!=0) {
				if(word.charAt(i)=='V') {
					number=Integer.parseInt(word.substring(i+1,i+pos[i]+1));
					sb.append(table1[number-1]);
				}else
				{
					number=Integer.parseInt(word.substring(i+1,i+pos[i]+1));
					sb.append(table2[number-1]);
				}
			}
		}
		
		return sb+"";
	}
    
}


