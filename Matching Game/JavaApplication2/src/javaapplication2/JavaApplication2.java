package javaapplication2;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.Random;

public class JavaApplication2 extends JFrame implements ActionListener {
    
   static String files[] = {"caillou", "daddy",
        "mommy", "Cat",
        "cow", "hello"};
    static JButton buttons[];
    String closedIcon = "[xxxxxx]";
    int numButtons;
    String icons[];
   // Timer myTimer;
    
    int numClicks = 0;
    int oddClickIndex = 0;
    int currentIndex = 0;

    public JavaApplication2() {
        // Set the title.

        setTitle("Memory Game");

        // Specify an action for the close button.
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create a BorderLayout manager.
        setLayout(new GridLayout(2, files.length));

       // closedIcon = closedIcon.setText("[xxxxxx]");
        numButtons = files.length * 2;
        buttons = new JButton[numButtons];
       // icons = icons[numButtons];
        for (int i = 0, j = 0; i < files.length; i++) {
            icons[j] = files[i];
            buttons[j] = new JButton("");
            buttons[j].addActionListener(this);
            buttons[j].setText("[xxxxxx]");
            add(buttons[j++]);

            icons[j] = icons[j - 1];
            buttons[j] = new JButton("");
            buttons[j].addActionListener(this);
            buttons[j].setText("[xxxxxx]");
            add(buttons[j++]);
        }

        // randomize icons array
        Random gen = new Random();
        for (int i = 0; i < numButtons; i++) {
            int rand = gen.nextInt(numButtons);
            String temp = icons[i];
            icons[i] = icons[rand];
            icons[rand] = temp;
        }

        // Pack and display the window.
        pack();
        setVisible(true);

       // myTimer = new Timer(1000, new TimerListener());
        // myTimer.start();
    }

  /*  private class TimerListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            buttons[currentIndex].setText("[xxxxxx]");
            buttons[oddClickIndex].setText("[xxxxxx]");;
           // myTimer.stop();
        }
    }*/

   // private class ImageButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            
            numClicks++;
            System.out.println(numClicks);
            
            // which button was clicked?
            for (int i = 0; i < numButtons; i++) {
                if (e.getSource() == buttons[i]) {
                    buttons[i].setText(icons[i]);
                    currentIndex = i;
                }
            }
            
            // check for even click
            if (numClicks % 2 == 0) {
                // check whether same position is clicked twice!
                if (currentIndex == oddClickIndex) {
                    numClicks--;
                    return;
                }
                // are two images matching?
                if (icons[currentIndex] != icons[oddClickIndex]) {
                    // show images for 1 sec, before flipping back
                   // myTimer.start(); 
                }
            } else {
                // we just record index for odd clicks
                oddClickIndex = currentIndex;
            }
        } 
   // }
    public static void main(String[] args) {
       
        new JavaApplication2();
    }
    
}



